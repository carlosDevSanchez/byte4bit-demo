const movieService = require('../services/movie.service');

class MovieController {

    async getMovies(pageSize, page, search, res) {
        try {
            const data = await movieService.getMovies(pageSize, page, search);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async getMovie(id, res) {
        try {
            const data = await movieService.getMovie(id);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async createMovie(newMovie, res) {
        try {
            const data = await movieService.createMovie(newMovie);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async publishedMovie(id, res) {
        try {
            const data = await movieService.publishedMovie(id);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async updateMovie(movie, res) {
        try {
            const data = await movieService.updateMovie(movie);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async deleteMovie(id, res) {
        try {
            const confirm = await movieService.deleteMovie(id);
            if (confirm) {
                return res.status(200).json({ message: "OK" });
            }
        } catch (err) {
            return res.status(500).send(err);
        }
    }
}
module.exports = new MovieController();