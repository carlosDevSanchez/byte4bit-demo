const userService = require('../services/user.service');
const { compareHash, createToken } = require("../middleware/auth");

class UserController {

    async getUsers(res) {
        try {
            const data = await userService.getUsers();
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async getUser(id, res) {
        try {
            const data = await userService.getUser(id);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async createUser(newUser, res) {
        try {
            const data = await userService.createUser(newUser);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async sigin(data, res) {
        try{
            const user = await userService.sigin(data.email);
            if(!user){
                return res.status(404).send('user not found');
            }
            if(!compareHash(user.password, data.password)){
                return res.status(401).send('credentials invalid');
            }

            const token = createToken(user.role, user._id);
            return res.status(200).json({ data: user, token, message: "OK" })
        }catch(e){
            return res.status(500).send(err);
        }
    }

    async updateUser(update, res) {
        try {
            const data = await userService.updateUser(update);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async deleteUser(id, res) {
        try {
            const confirm = await userService.deleteUser(id);
            if (confirm) {
                return res.status(200).json({ message: "OK" });
            }
        } catch (err) {
            return res.status(500).send(err);
        }
    }
}
module.exports = new UserController();