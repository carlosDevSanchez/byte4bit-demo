const commentService = require('../services/comment.service');

class CommentController {

    async getComments(id, limit, filterQuery, res) {
        
        try {
            const data = await commentService.getComments(id, limit, filterQuery);
            const next = data[data.length - 1]._id;
            return res.json({ data, next, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async getCommentByUser(id, res) {
        try {
            const data = await commentService.getCommentByUser(id);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async getComment(id, res) {
        try {
            const data = await commentService.getComment(id);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async createComment(newComment, res) {
        try {
            const data = await commentService.createComment(newComment);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async updateComment(updatedata, res) {
        try {
            const data = await commentService.updateComment(updatedata);
            return res.json({ data, message: "OK" });
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    async deleteComment(commentId, res) {
        try {
            const confirm = await commentService.deleteComment(commentId);
            if(confirm){
                return res.status(200).json({ message: "OK" });
            }
        } catch (err) {
            return res.status(500).send(err);
        }
    }
}
module.exports = new CommentController();