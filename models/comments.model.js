const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
        comment: {
            type: String
        },
        rating: {
            type: String
        },
        movie: {
            ref: "Movies",
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        userId: {
            ref: "Users",
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        createAt: {
            type: Date,
        },
        updatedAt: {
            type: Date,
        },
    },
    { timestamps: { createAt: 'created_at', updatedAt: 'updated_at' } }
);

const Comment = mongoose.model('Comments', commentSchema);

module.exports = {
    Comment
}