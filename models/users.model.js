const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    role: {
        type: String,
        default: "USER",
        enum: ["USER", "ADMIN"],
    },
    createAt: {
        type: Date,
    },
    updatedAt: {
        type: Date,
    },
},
    { timestamps: { createAt: 'created_at', updatedAt: 'updated_at' } }
);

const User = mongoose.model('Users', userSchema);
module.exports = {
    User,
}