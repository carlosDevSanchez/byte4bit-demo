const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema({
        title: {
            type: String
        },
        userId: {
            ref: "Users",
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        published: {
            type: Boolean,
            default: false,
        },
        createAt: {
            type: Date,
        },
        updatedAt: {
            type: Date,
        },
    },
    { timestamps: { createAt: 'created_at', updatedAt: 'updated_at' } }
);

const Movie = mongoose.model('Movies', movieSchema);

module.exports = {
    Movie
}