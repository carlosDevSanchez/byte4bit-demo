const express = require('express');
const cors = require("cors");
require('dotenv').config();
const swaggerFile = require('./swagger-output.json');
const swaggerUi = require('swagger-ui-express');
const Routes = require("./routes/index");
const { generateUserAdmin } = require('./services/user.service');

const app = express();
const port = process.env.PORT || 4000;

app.use(express.json());
app.use(cors());
generateUserAdmin();
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use('/v1', Routes);


app.listen(port, () => {
    console.log(`Server listening on the port  ${port}`);
})