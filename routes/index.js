const { Router } = require("express");
const route = Router();

const MoviesRoutes = require("./movie.route");
const CommentsRoutes = require("./comment.route");
const UsersRoutes = require("./users.route");

route.use("/", MoviesRoutes);
route.use("/", CommentsRoutes);
route.use("/", UsersRoutes);

module.exports = route;