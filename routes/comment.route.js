const { Router } = require("express");
const commentController = require('../controllers/comment.controller');
const { authenticateToken, ROLES } = require("../middleware/auth");

const router = Router();

router.get('/api/comments/movie/:id', async (req, res) => {
    const limit = req.query.limit || 1;
    const filterQuery = {};
    if (req.query.next) {
        filterQuery._id = {$lt: ObjectId(req.query.next)}
    }

    return await commentController.getComments(req.params.id, limit, filterQuery, res);
});

router.get('/api/comment/user/:id', authenticateToken, async (req, res) => {
    if(req.user.role !== ROLES.ADMIN){
        return res.status(401).send("Unauthorize");
    }
    
    const id = req.params.id || "";
    return await commentController.getCommentByUser(id, res);
});

router.get('/api/comment/:id', async (req, res) => {
    const id = req.params.id || "";
    return await commentController.getComment(id, res);
});

router.post('/api/comment', authenticateToken, async (req, res) => {
    const {
        comment,
        rating,
        movie,
        userId,
    } = req.body;

    const newComment = {
        comment,
        rating,
        movie,
        userId,
    }

    return await commentController.createComment(newComment, res);
});

router.put('/api/comment/:id', authenticateToken, async (req, res) => {
    const {
        comment,
        rating,
        movieId,
        userId,
    } = req.body;

    const update = {
        id: req.params.id,
        comment,
        rating,
        movieId,
        userId,
    }

    return await commentController.updateComment(update, res);
});

router.delete('/api/comment/:id', authenticateToken, async (req, res) => {
    return await commentController.deleteComment(req.params.id, res);
});

module.exports = router;