const { Router } = require("express");
const movieController = require('../controllers/movie.controller');
const { authenticateToken, ROLES } = require("../middleware/auth");

const router = Router();

router.get('/api/movies', async (req, res) => {
    const pageSize = req.query.pageSize || 0;
    const page = req.query.page || 0;
    const search = req.query.search || "";

    return await movieController.getMovies(pageSize, page, search, res);
});

router.get('/api/movie/:id', async (req, res) => {
    const id = req.params.id || "";
    return await movieController.getMovie(id, res);
});

router.post('/api/movie', authenticateToken, async (req, res) => {
    const {
        title,
        published,
        userId,
    } = req.body;

    if(req.user.role !== ROLES.ADMIN && published === true){
        return res.status(401).send("Unauthorize");
    }

    const newMovie = {
        title,
        published: published,
        userId,
    }

    return await movieController.createMovie(newMovie, res);
});

router.post('/api/movie/published/:id', authenticateToken, async (req, res) => {
    if(req.user.role !== ROLES.ADMIN){
        return res.status(401).send("Unauthorize");
    }

    return await movieController.publishedMovie(req.params.id, res);
});

router.put('/api/movie/:id', authenticateToken, async (req, res) => {
    const {
        title,
        published,
        userId,
    } = req.body;

    const update = {
        id: req.params.id,
        title,
        published,
        userId,
    }

    return await movieController.updateMovie(update, res);
});

router.delete('/api/movie/:id', authenticateToken, async (req, res) => {
    return await movieController.deleteMovie(req.params.id, res);
});

module.exports = router;