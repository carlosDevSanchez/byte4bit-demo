const { Router } = require("express");
const userController = require('../controllers/user.controller');
const { authenticateToken, ROLES, hashPassword } = require("../middleware/auth");

const router = Router();

router.get('/api/users', authenticateToken, async (req, res) => {
    if(req.user?.role !== ROLES.ADMIN){
        return res.status(401).send("Unauthorize");
    }

    return await userController.getUsers(res);
});

router.get('/api/user/:id', authenticateToken, async (req, res) => {
    const id = req.params.id || "";
    return await userController.getUser(id, res);
});

router.post('/api/user', authenticateToken, async (req, res) => {
    if(req.user?.role !== ROLES.ADMIN){
        return res.status(401).send("Unauthorize");
    }

    const {
        name,
        email,
        password,
        role,
    } = req.body;

    const newUser = {
        name,
        email,
        password: hashPassword(password),
        role,
    }

    return await userController.createUser(newUser, res);
});

router.post('/api/register', async (req, res) => {
    const {
        name,
        email,
        password,
        role,
    } = req.body;

    if (role === ROLES.ADMIN) {
        return res.status(401).send('Unauthorize create admin user');
    }

    const newUser = {
        name,
        email,
        password: await hashPassword(password),
        role,
    }
    return await userController.createUser(newUser, res);
});

router.post('/api/signin', async (req, res) => {
    const {
        email,
        password,
    } = req.body;
    const data = { email, password };

    return await userController.sigin(data, res);
});

router.put('/api/user', authenticateToken, async (req, res) => {
    const {
        name,
        email,
        role,
    } = req.body;

    if (role === ROLES.ADMIN && req.user.role !== ROLES.ADMIN) {
        return res.status(401).send('Unauthorize');
    }

    const update = {
        id: req.user.id,
        name,
        email,
        role,
    }

    return await userController.updateUser(update, res);
});

router.delete('/api/user/:id', authenticateToken, async (req, res) => {
    if (req.user.role !== ROLES.ADMIN) {
        return res.status(401).send('Unauthorize');
    }

    return await userController.deleteUser(req.params.id, res);
});

module.exports = router;