const swaggerAutogen = require('swagger-autogen')()

const doc = {
    info: {
        version: "0.0.1",
        title: "API REST Byte 4bit",
        description: "API REST demo prueba Byte 4bit",
    },
    host: "localhost:4001",
    basePath: "/",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
}

const outputFile = './swagger-output.json';
const endpointsFiles = ['./server.js'];

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./server.js');
});