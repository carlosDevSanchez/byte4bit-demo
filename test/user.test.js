const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:4001/v1/api';

describe('POST register user: ', () => {
    it('register users', (done) => {
        chai.request(url)
            .post('/register')
            .send({ name: "jhon doe", role: "USER", email: "jhondoe@gmail.com", password: "tester" })
            .end(function (err, res) {
                expect(res.body).to.have.property("message");
                expect(res.body.message).to.equal("OK");

                done();
            });
    });
});

describe('POST sigin user: ', () => {
    it('login user email and password', (done) => {
        chai.request(url)
            .post('/signin')
            .send({ email: "jhondoe@gmail.com", password: "tester" })
            .end(function (err, res) {
                expect(res.body).to.have.property('token');
                done();
            });
    });
});


describe('GET users with admin user: ', () => {
    it('users with role admin', (done) => {
        chai.request(url)
        .post('/signin')
        .send({ email: "admin@byte4bit.com", password: "admin123" })
        .end(function (err, res) {
            expect(res.body).to.have.property('token');
            const token = res.body.token;

            chai.request(url)
                .get('/users')
                .set('Authorization', `Bearer ${token}`)
                .end(function (err, res) {
                    expect(res.body).to.have.property("message");
                    expect(res.body.message).to.equal("OK");

                    done();
                });
        });
        
    });
});


describe('DELETE users with admin user: ', () => {
    it('users with role admin', (done) => {
        chai.request(url)
        .post('/signin')
        .send({ email: "admin@byte4bit.com", password: "admin123" })
        .end(function (err, res) {
            expect(res.body).to.have.property('token');
            const token = res.body.token;

            chai.request(url)
                .get('/users')
                .set('Authorization', `Bearer ${token}`)
                .end(function (err, res) {
                    expect(res.body).to.have.property("message");
                    expect(res.body.message).to.equal("OK");

                    chai.request(url)
                        .delete(`/user/${res.body.data.find(r => r.email === 'jhondoe@gmail.com')._id}`)
                        .set('Authorization', `Bearer ${token}`)
                        .end(function (err, res) {
                            expect(res.body).to.have.property("message");
                            expect(res.body.message).to.equal("OK");

                            done();
                        });
                });
        });
        
    });
});
