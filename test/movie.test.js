const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:4001/v1/api';

describe('POST movie test', () => {
    it('register new movie', (done) => {
        chai.request(url)
            .post('/signin')
            .send({
                'email': 'admin@byte4bit.com',
                'password': 'admin123'
            })
            .end((err, res) => {
                expect(res.body).to.have.property('token');
                const token = res.body.token;

                chai.request(url)
                    .post('/movie')
                    .send({
                        title: "Marvel comic",
                        userId: res.body.data._id,
                        published: true,
                    })
                    .set('Authorization', `Bearer ${token}`)
                    .end(function (error, resp) {
                        expect(resp.body).to.have.property("message");
                        expect(resp.body.message).to.equal("OK");

                        done();
                    });
            })
    })
})

describe('PUT movie test', () => {
    it('update movie', (done) => {
        chai.request(url)
            .post('/signin')
            .send({
                'email': 'admin@byte4bit.com',
                'password': 'admin123'
            })
            .end((err, res) => {
                expect(res.body).to.have.property('token');
                const token = res.body.token;

                chai.request(url)
                .get('/movies')
                .query({ pageSize: 0, page: 1 })
                .end(function (err, resMovie) {
                    expect(resMovie.body).to.have.property("message");
                    expect(resMovie.body.message).to.equal("OK");

                    chai.request(url)
                        .put('/movie/' + resMovie.body.data[0]._id)
                        .send({
                            title: "Marvel comic modificado",
                            userId: res.body._id,
                            published: true,
                        })
                        .set('Authorization', `Bearer ${token}`)
                        .end(function (error, resp) {
                            expect(resp.body).to.have.property("message");
                            expect(resp.body.message).to.equal("OK");
                            done();
                        });
                })
            })
    })
})

describe('GET movies all with pagination and get movie with ID: ', () => {
    it('should get movies paginate', (done) => {
        chai.request(url)
            .get('/movies')
            .query({ pageSize: 0, page: 1 })
            .end(function (err, res) {
                expect(res.body).to.have.property("message");
                expect(res.body.message).to.equal("OK");

                chai.request(url)
                    .get(`/movie/${res.body.data[0]._id}`)
                    .end(function (err, resOne) {
                        expect(resOne.body).to.have.property("message");
                        expect(resOne.body.message).to.equal("OK");

                        done();
                    });
            });
    });
});

describe('DELETE movie test', () => {
    it('update movie', (done) => {
        chai.request(url)
            .post('/signin')
            .send({
                'email': 'admin@byte4bit.com',
                'password': 'admin123'
            })
            .end((err, res) => {
                expect(res.body).to.have.property('token');
                const token = res.body.token;

                chai.request(url)
                .get('/movies')
                .query({ pageSize: 0, page: 1 })
                .end(function (err, resMovie) {
                    expect(resMovie.body).to.have.property("message");
                    expect(resMovie.body.message).to.equal("OK");

                    chai.request(url)
                        .delete('/movie/' + resMovie.body.data[0]._id)
                        .set('Authorization', `Bearer ${token}`)
                        .end(function (error, resp_delete) {
                            expect(resp_delete.body).to.have.property("message");
                            expect(resp_delete.body.message).to.equal("OK");

                            done();
                        });
                })
            })
    })
})



