const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:4001/v1/api';


describe('POST movie and create comment', () => {
    it('register new movie and new comment', (done) => {
        chai.request(url)
            .post('/signin')
            .send({
                'email': 'admin@byte4bit.com',
                'password': 'admin123'
            })
            .end((err, res) => {
                expect(res.body).to.have.property('token');
                const token = res.body.token;

                chai.request(url)
                    .post('/movie')
                    .send({
                        title: "Marvel comic",
                        userId: res.body.data._id,
                        published: true,
                    })
                    .set('Authorization', `Bearer ${token}`)
                    .end(function (error, resp) {
                        expect(resp.body).to.have.property("message");
                        expect(resp.body.message).to.equal("OK");

                        chai.request(url)
                            .post("/comment")
                            .send({
                                comment: "Buena peli!",
                                rating: "5",
                                movie: resp.body.data._id,
                                userId: res.body.data._id,
                            })
                            .set('Authorization', `Bearer ${token}`)
                            .end(function (error, respComment) {
                                expect(respComment.body).to.have.property("message");
                                expect(respComment.body.message).to.equal("OK");


                                done();
                            });
                    });
            })
    })
})

describe('PUT update comment', () => {
    it('update comment', (done) => {
        chai.request(url)
            .post('/signin')
            .send({
                'email': 'admin@byte4bit.com',
                'password': 'admin123'
            })
            .end((err, res) => {
                expect(res.body).to.have.property('token');
                const token = res.body.token;

                chai.request(url)
                    .get(`/comment/user/${res.body.data._id}`)
                    .set('Authorization', `Bearer ${token}`)
                    .end(function (error, resp) {
                        expect(resp.body).to.have.property("message");
                        expect(resp.body.message).to.equal("OK");

                        chai.request(url)
                            .put(`/comment/${resp.body.data[0]._id}`)
                            .send({
                                comment: "Buena peli modificado!",
                                rating: "2",
                                movie: resp.body.data._id,
                                userId: res.body.data._id,
                            })
                            .set('Authorization', `Bearer ${token}`)
                            .end(function (error, respComment) {
                                expect(respComment.body).to.have.property("message");
                                expect(respComment.body.message).to.equal("OK");

                                done();
                            });
                    });
            })
    })
})


describe('DELETE delete comment', () => {
    it('delete comment', (done) => {
        chai.request(url)
            .post('/signin')
            .send({
                'email': 'admin@byte4bit.com',
                'password': 'admin123'
            })
            .end((err, res) => {
                expect(res.body).to.have.property('token');
                const token = res.body.token;

                chai.request(url)
                    .get(`/comment/user/${res.body.data._id}`)
                    .set('Authorization', `Bearer ${token}`)
                    .end(function (error, resp) {
                        expect(resp.body).to.have.property("message");
                        expect(resp.body.message).to.equal("OK");

                        chai.request(url)
                            .delete(`/comment/${resp.body.data[0]._id}`)
                            .set('Authorization', `Bearer ${token}`)
                            .end(function (error, respComment) {
                                expect(respComment.body).to.have.property("message");
                                expect(respComment.body.message).to.equal("OK");

                                done();
                            });
                    });
            })
    })
})
