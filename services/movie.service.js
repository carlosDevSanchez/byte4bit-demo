const { connect } = require("../config/db");
const Promise = require("bluebird");
const { Movie } = require("../models/movie.model");
const { Comment } = require("../models/comments.model");
const logger = require("../logger/api.logger");

class MovieService {

    constructor() {
        connect();
    }

    async getMovies(pageSize, page, search) {
        let query = {}
        if (search) {
            query = {
                title: { $regex: search, $options: "i" }
            }
        }

        try {
            const movies = await Movie.find({ ...query, published: true })
                .populate("userId").limit(pageSize).skip(pageSize * page);

            const data = [];
            await Promise.map(movies, async movie => {
                const comments = await Comment.find({ movie: movie._id }).populate('userId');
                const average = comments.reduce((prev, row) => prev + row.rating, 0);
                data.push({
                    ...movie._doc,
                    comments,
                    ratingAverage: (average / comments.length)
                })
            })

            return data;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async getMovie(id) {
        try {
            const movie = await Movie.findOne({ _id: id }).populate("userId");
            const comments = await Comment.find({ movie: movie._id }).populate('userId');
            const average = comments.reduce((prev, row) => prev + row.rating, 0);

            return {
                ...movie._doc,
                ratingAverage: (average / comments.length)
            };
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async createMovie(newMovie) {
        try {
            const movie = new Movie(newMovie);
            await movie.save();

            return movie;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async publishedMovie(id) {
        try {
            const movie = await Movie.findByIdAndUpdate(id, { published: true });
            return movie;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async updateMovie(data) {
        try {
            const movie = await Movie.findByIdAndUpdate(data.id, data);
            return movie;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async deleteMovie(movieId) {
        try {
            await Movie.deleteOne({ _id: movieId });
            return true;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

}

module.exports = new MovieService();