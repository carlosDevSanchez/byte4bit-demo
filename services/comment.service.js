const { connect } = require("../config/db");
const { Comment } = require("../models/comments.model");
const logger = require("../logger/api.logger");

class CommentService {

    constructor() {
        connect();
    }

    async getComments(id, limit, filterQuery) {
        try {
            const comments = await Comment.find({ ...filterQuery, movie: id })
                .populate("userId").sort({ _id: -1 }).limit(limit);
            return comments;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async getComment(id) {
        try {
            const comment = await Comment.findOne({ _id: id }).populate("userId");
            return comment;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async getCommentByUser(id){
        try {
            const comments = await Comment.find({ userId: id }).populate("userId");
            return comments;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async createComment(newComment) {
        try {
            const comment = new Comment(newComment);
            await comment.save();

            return comment;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async updateComment(data) {
        try {
            const comment = await Comment.findByIdAndUpdate(data.id, data);
            return comment;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async deleteComment(commentId) {
        try {
            await Comment.deleteOne({ _id: commentId });
            return true;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

}

module.exports = new CommentService();