const { connect } = require("../config/db");
const { User } = require("../models/users.model");
const { hashPassword } = require("../middleware/auth");
const logger = require("../logger/api.logger");

class UserService {

    constructor() {
        connect();
    }

    async generateUserAdmin() {
        try {
            const result = await User.findOne({ role: "ADMIN" });
            if (result) {
                return;
            }

            const data = {
                name: "Admin",
                email: "admin@byte4bit.com",
                password: await hashPassword("admin123"),
                role: "ADMIN",
            }
            const user = new User(data)
            await user.save();
            logger.info("user admin create", user);
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async getUsers() {
        try {
            const users = await User.find({}).select('+password');
            return users;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async getUser(id) {
        try {
            const user = await User.findOne({ _id: id }).select('+password');
            return user;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async createUser(newUser) {
        try {
            const user = new User(newUser);
            await user.save();
            user.password = null;
            return user;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async sigin(email) {
        try {
            const user = await User.findOne({ email });
            return user;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async updateUser(data) {
        try {
            const user = await User.findByIdAndUpdate(data.id, data);
            user.password = null;
            return user;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

    async deleteUser(userId) {
        try {
            await User.deleteOne({ _id: userId });
            return true;
        } catch (err) {
            logger.error('Error::' + err);
        }
    }

}

module.exports = new UserService();