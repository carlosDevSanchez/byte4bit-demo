const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const SALT = 10;

const secretKey = process.env.SECRET_KEY;

const authenticateToken = (req, res, next) => {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        jsonwebtoken.verify(req.headers.authorization.split(' ')[1], secretKey, function (err, decode) {
            if (err) req.user = undefined;
            
            req.user = decode;
            next();
        });
    } else {
        req.user = undefined;
        next();
    }
};

const createToken = (role, id) => {
    try {
        const token = jsonwebtoken.sign({ id, role }, secretKey, { expiresIn: '1 years' })
        return token;
    } catch (e) {
        throw new Error('Error to generate token')
    }
}

const hashPassword = async (password) => {
    return await bcrypt.hash(password, SALT);
}

const compareHash = async (hash, password) => {
    return await bcrypt.compare(password, hash);
}

const ROLES = {
    ADMIN: "ADMIN",
    USER: "USER"
};

module.exports = { authenticateToken, createToken, hashPassword, compareHash, ROLES }